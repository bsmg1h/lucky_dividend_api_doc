# Lucky 平台分红计算接口

---  

## 安装  

```  
$ git clone https://bsmg1h@bitbucket.org/bsmg1h/lucky_dividend_api.git
```  

---  

## 接口地址  
  
internal ip（内网地址）: http://192.168.8.52:8876/computeDividend    
  
external ip（外网地址）: http://210.3.80.74:8876/computeDividend    

均为post方法

---

## 参数说明

- data: 用户数据集，json， {id_0: [存币量_0, 流水量_0，vip等级_0]，id_1: [存币量_1, 流水量_1，vip等级_1] ...}, 可参见simulated_data/simulated_data_0.json 
- a: 用户存币的指数参数，float， 范围0.25～0.75
- b: 用户流水量的指数参数，float， 范围0.25～0.75
- vip_level_para: 每个vip等级的奖励系数，具体格式参见simulated_data/vip_level_para.json
- daily_profit: lucky平台的当日收益
- avg_lucky_price: lucky币的当日平均价格，单位？
- dividend_ratio: 分红比例，将当日一定比例的收益作为总分红量


---

## 返回格式

json，{id_0：lucky分红数量_0，id_1：lucky分红数量_1，...}


---

## 接口调用范例

参见test.py

确定接口地址后可在test.py中进行修改，执行python test.py 进行测试


---


# 分红权重函数

下图可以简要看出指数参数的变化对分红比例分布变化的影响

横坐标为存币量，总坐标为流水量，a为存币量的指数参数，b为流水量的指数参数，颜色代表处于该位置的玩家相对分红权重大小  

- 希望鼓励存币：提升a会让存币量大的玩家分红更高，显著提升存币量>>流水量的玩家的分红，显著降低存币量<<流水量的玩家的分红  
- 希望鼓励流水：提升b会让流水量大的玩家分红更高，显著提升流水量>>存币量的玩家的分红，显著降低流水量<<存币量的玩家的分红  
- 希望影响长尾玩家、新用户：同时提升、降低ab会显著降低、提升低端玩家（存币量或流水量较低的玩家）的分红，对高端玩家（存币量与流水量均较大的玩家）作用不明显  

![分红比例分布变化](https://bitbucket.org/bsmg1h/lucky_dividend_api/raw/1fce755077ac570ae580acaf2f0fcdd8ef768b8c/sample.png)
