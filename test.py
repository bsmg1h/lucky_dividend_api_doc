import json
import requests

with open('simulated_data/vip_level_para.json', 'r', encoding='utf-8') as fp:
    vip_level_para = json.load(fp)

with open('simulated_data/simulated_data_1.json', 'r', encoding='utf-8') as fp:
    data = json.load(fp)

# ans = compute_dividend(data=data,
#                        a=1,
#                        b=1,
#                        vip_level_para=vip_level_para,
#                        daily_profit=10000,
#                        avg_lucky_price=0.1,
#                        dividend_ratio=0.01)

payloads = {
    'data': data,
    'a': 1,
    'b': 1,
    'vip_level_para': vip_level_para,
    'daily_profit': 10000,
    'avg_lucky_price': 0.1,
    'dividend_ratio': 0.01
}


a = requests.post('http://210.3.80.74:8876/computeDividend', json=payloads)
print(a.json())
